const {
    UserController,
    BookController,
    AuthorController,
    PublisherController,
  } = require('../controller');
  
  const { AuthMiddleWare } = require('../middleware');
  
  module.exports = (app) => {

    // login & register handler
    app.post('/login', UserController.login);
    app.post('/register', UserController.createUser);

    // Book API
    app.get('/books', BookController.listBook);
    app.post('/book', AuthMiddleWare(), BookController.createBook);
    app.put('/book/:bookId', AuthMiddleWare(), BookController.updateBook);
    app.delete('/book/:bookId', AuthMiddleWare(), BookController.deleteBook);

    // Author API
    app.get('/authors', AuthorController.listAuthor);
    app.post('/author', AuthMiddleWare(), AuthorController.createAuthor);
    app.put('/author/:authorId', AuthMiddleWare(), AuthorController.updateAuthor);
    app.delete('/author/:authorId', AuthMiddleWare(), AuthorController.deleteAuthor);

    // Pubsliher API
    app.get('/publishers', PublisherController.listPublisher);
    app.post('/publisher', AuthMiddleWare(), PublisherController.createPublisher);
    app.put('/publisher/:publisherId', AuthMiddleWare(), PublisherController.updatePublisher);
    app.delete('/publisher/:publisherId', AuthMiddleWare(), PublisherController.deletePublisher);
  };
  