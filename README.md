# Simple REST API

A simple REST API. MVP - Node (Express)

Install all modules
```
    yarn install
```

For first running, you need to migrate and seed the data. You can run this command
```
    yarn migrate | yarn seeds
```

Run app
```
    yarn start
```

How to Add new API

- add new function for handler on Controller
- add new endpoint to router/index.js
- rerun the app

How to Add new Controller:
- Duplicate on of existing controller & rename it
- Register the controller at controller/index.js