'use strict';

/** @type {import('sequelize-cli').Migration} */

const bcrypt = require("bcrypt");

module.exports = {
  async up (queryInterface, Sequelize) {
    let users = [
      {
        firstName: 'John',
        lastName: 'Doe',
        email: 'example@example.com',
        password: '123123',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        firstName: 'Abdoel',
        lastName: null,
        email: 'abdoel@example.com',
        password: '123123123',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        firstName: 'Michael',
        lastName: 'Johnathan',
        email: 'john@example.com',
        password: '123123',
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ]

    for (let i = 0; i < users.length; i++) {
      let user = users[i];
      // generate salt to hash password
      const salt = await bcrypt.genSalt(10);
      // hash password
      user.password = await bcrypt.hash(user.password, salt);
    }

    await queryInterface.bulkInsert('Users', users);
  },

  async down (queryInterface, Sequelize) {
     await queryInterface.bulkDelete('Users', null, {});
  }
};
