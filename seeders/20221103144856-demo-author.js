'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    const authors = [
      {
        name: 'John doe',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'John Whitfield',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Drew Dudley',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]

    await queryInterface.bulkInsert('Authors', authors);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Authors', null, {});
  }
};
