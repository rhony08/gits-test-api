'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    const publishers = [
      {
        name: 'Duckworth Overlook',
        address: '30 Calvin Street, London E1 6NW',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Kompas Gramedia',
        address: 'Jakarta Selatan',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]

    await queryInterface.bulkInsert('Publishers', publishers);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Publishers', null, {});
  }
};
