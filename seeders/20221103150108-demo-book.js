'use strict';

const db = require('../models');

/** @type {import('sequelize-cli').Migration} */

module.exports = {
  async up (queryInterface, Sequelize) {
    const authors = await db['Author'].findAll()
    const publishers = await db['Publisher'].findAll()

    let books = [
      {
        title: 'People Will Talk',
        isbn: '978-0715645222',
        authorId: 2,
        publisherId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: 'Workstrorming',
        isbn: '978-1780289175',
        authorId: 3,
        publisherId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]

    for (let index = 0; index < books.length; index++) {
      const element = books[index];
      const randomIdxAuthor = await Math.floor(await Math.random() * (authors.length - 1));
      const randomIdxPublisher = await Math.floor(await Math.random() * (publishers.length - 1));
      element.authorId = authors[randomIdxAuthor].id
      element.publisherId = publishers[randomIdxPublisher].id
    }

    await queryInterface.bulkInsert('Books', books);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Books', null, {});
  }
};
