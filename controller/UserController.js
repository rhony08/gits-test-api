const db = require('../models')
const jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");

module.exports = {
  createUser: async (req, res, next) => {
    try {
      const { email, firstName, lastName, password } = req.body;

      // generate salt to hash password
      const salt = await bcrypt.genSalt(10);
      // hash password
      const pwd = await bcrypt.hash(password, salt);

      let user = await db['User'].create({email, firstName, lastName, password: pwd});
      res.send(user);
    } catch (err){
      next(err);
    }
  },

  login: async (req, res, next) => {
    try {
      const { email, password } = req.body;

      let user = await db['User'].findOne({
        where:{
          email: email,
        }
      });
      if (user) {
        // check user password with hashed password stored in the database
        const validPassword = await bcrypt.compare(password, user.password);
        if (validPassword) {
          const payload = {
            id: user.id
          };
    
          const token = await jwt.sign(payload, process.env.SECRET_KEY, {
            expiresIn: 24 * 60 * 60 // set expire time for token on the next 24 hours
          });

          res.status(200).json({ message: "Valid password", success: true, token });
        } else {
          res.status(400).json({ message: "Invalid Password", success: false });
        }
      } else {
        res.status(401).json({ error: "User does not exist", success: false });
      }
    } catch (err){
      next(err);
    }
  },
}