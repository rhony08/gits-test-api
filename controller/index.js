const UserController = require('./UserController');
const BookController = require('./BookController');
const AuthorController = require('./AuthorController');
const PublisherController = require('./PublisherController');

module.exports = {
  UserController,
  BookController,
  AuthorController,
  PublisherController,
};
