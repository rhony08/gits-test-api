const db = require('../models');
const { Op } = require("sequelize");

module.exports = {
  listBook: async (req, res, next) => {
    try {
      let { page, size, name } = req.query;

      if (!size) {
        size = 10
      }

      let offset = 0;

      if (page) {
        offset = (page-1) * size
      }

      let reqParam = {
        offset,
        limit: size,
        where: { deletedAt: { [Op.is]: null } }
      }

      if (name) {
        reqParam['where']['title'] = {
          [Op.iLike]: `%${name}%`,
        }
      }

      let books = await db['Book'].findAll(reqParam);
      res.send(JSON.stringify(books));
    } catch (err){
      next(err);
    }
  },
  createBook: async (authenticatedUser, req, res, next) => {
    try {
      const {title, isbn, authorId, publisherId} = req.body;
      let book = await db['Book'].create({title, isbn, authorId, publisherId});
      res.send(JSON.stringify(book));
    } catch (err){
      next(err);
    }
  },
  updateBook: async (authenticatedUser, req, res, next) => {
    try {
      const {bookId} = req.params;
      const {title, isbn, authorId, publisherId} = req.body;
      let book = await db['Book'].update(
        {title, isbn, authorId, publisherId},
        { where : { id : bookId } }
      );
      let message;
      // check updated rows, since by id should be only 1 data will update
      if (book && Array.isArray(book) && book.length > 0 && book[0] == 1) {
        message = 'Successfully update book'
      } else {
        message = 'Failed update book'
      }

      res.send({'message': message});
    } catch (err){
      next(err);
    }
  },
  deleteBook: async (authenticatedUser, req, res, next) => {
    try {
      const {bookId} = req.params;
      let book = await db['Book'].update(
        {deletedAt: new Date()},
        { where : { id : bookId } }
      );

      let message;
      // check updated rows, since by id should be only 1 data will update
      if (book && Array.isArray(book) && book.length > 0 && book[0] == 1) {
        message = 'Successfully delete book'
      } else {
        message = 'Failed delete book'
      }

      res.send({'message': message});
    } catch (err){
      next(err);
    }
  },
}