const db = require('../models');
const { Op } = require("sequelize");

module.exports = {
  listAuthor: async (req, res, next) => {
    try {
      let { page, size, name } = req.query;

      if (!size) {
        size = 10
      }

      let offset = 0;

      if (page) {
        offset = (page-1) * size
      }

      let reqParam = {
        offset,
        limit: size,
        where: { deletedAt: { [Op.is]: null } }
      }

      if (name) {
        reqParam['where']['name'] = {
          [Op.iLike]: `%${name}%`,
        }
      }

      let authors = await db['Author'].findAll(reqParam);
      res.send(JSON.stringify(authors));
    } catch (err){
      next(err);
    }
  },
  createAuthor: async (authenticatedUser, req, res, next) => {
    try {
      const {name} = req.body;
      let author = await db['Author'].create({name});
      res.send(JSON.stringify(author));
    } catch (err){
      next(err);
    }
  },
  updateAuthor: async (authenticatedUser, req, res, next) => {
    try {
      const {authorId} = req.params;
      const {name} = req.body;
      let author = await db['Author'].update(
        {name},
        { where : { id : authorId } }
      );
      let message;
      // check updated rows, since by id should be only 1 data will update
      if (author && Array.isArray(author) && author.length > 0 && author[0] == 1) {
        message = 'Successfully update author'
      } else {
        message = 'Failed update author'
      }

      res.send({'message': message});
    } catch (err){
      next(err);
    }
  },
  deleteAuthor: async (authenticatedUser, req, res, next) => {
    try {
      const {authorId} = req.params;
      let author = await db['Author'].update(
        {deletedAt: new Date()},
        { where : { id : authorId } }
      );

      let message;
      // check updated rows, since by id should be only 1 data will update
      if (author && Array.isArray(author) && author.length > 0 && author[0] == 1) {
        message = 'Successfully delete author'
      } else {
        message = 'Failed delete author'
      }

      res.send({'message': message});
    } catch (err){
      next(err);
    }
  },
}