const db = require('../models');
const { Op } = require("sequelize");

module.exports = {
  listPublisher: async (req, res, next) => {
    try {
      let { page, size, name } = req.query;

      if (!size) {
        size = 10
      }

      let offset = 0;

      if (page) {
        offset = (page-1) * size
      }

      let reqParam = {
        offset,
        limit: size,
        where: { deletedAt: { [Op.is]: null } }
      }

      if (name) {
        reqParam['where']['name'] = {
          [Op.iLike]: `%${name}%`,
        }
      }

      let publishers = await db['Publisher'].findAll(reqParam);
      res.send(JSON.stringify(publishers));
    } catch (err){
      next(err);
    }
  },
  createPublisher: async (authenticatedUser, req, res, next) => {
    try {
      const {name, address} = req.body;
      let publisher = await db['Publisher'].create({name, address});
      res.send(JSON.stringify(publisher));
    } catch (err){
      next(err);
    }
  },
  updatePublisher: async (authenticatedUser, req, res, next) => {
    try {
      const {publisherId} = req.params;
      const {name, address} = req.body;
      let publisher = await db['Publisher'].update(
        {name, address},
        { where : { id : publisherId } }
      );
      let message;
      // check updated rows, since by id should be only 1 data will update
      if (publisher && Array.isArray(publisher) && publisher.length > 0 && publisher[0] == 1) {
        message = 'Successfully update publisher'
      } else {
        message = 'Failed update publisher'
      }

      res.send({'message': message});
    } catch (err){
      next(err);
    }
  },
  deletePublisher: async (authenticatedUser, req, res, next) => {
    try {
      const {publisherId} = req.params;
      let publisher = await db['Publisher'].update(
        {deletedAt: new Date()},
        { where : { id : publisherId } }
      );

      let message;
      // check updated rows, since by id should be only 1 data will update
      if (publisher && Array.isArray(publisher) && publisher.length > 0 && publisher[0] == 1) {
        message = 'Successfully delete publisher'
      } else {
        message = 'Failed delete publisher'
      }

      res.send({'message': message});
    } catch (err){
      next(err);
    }
  },
}