const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const http = require('http');

const cors = require('cors');

const Router = require('./router');

const PORT = process.env.PORT || 8002;

const app = express();

process.on('unhandledRejection', err => {});

// safety first
app.use(helmet());

// CORS Dev
const corsOptions = {
  origin: true,
  credentials: true
};

app.use(cors(corsOptions));
app.use(bodyParser.json());

// our server instance
const server = http.createServer(app)

Router(app);

app.use(( error , req, res, next) => {
  return res.status(error.code || 422).send({ 
    status: {
      status: error.code || 422,
      message: error.message,
      succeeded: false
    }});
});

server.listen(PORT, () => {
  console.log(`Listen on port ${PORT}`)
})

module.exports = app;