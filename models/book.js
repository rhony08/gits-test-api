'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Book extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Book.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    isbn: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    authorId: {
      type: DataTypes.INTEGER,
      references: {
        model: {
          tableName: 'Authors',
          schema: 'public'
        },
        key: 'id'
      },
      allowNull: false
    },
    publisherId: {
      type: DataTypes.INTEGER,
      references: {
        model: {
          tableName: 'Publishers',
          schema: 'public'
        },
        key: 'id'
      },
      allowNull: false
    },
    deletedAt: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'Book',
  });
  return Book;
};