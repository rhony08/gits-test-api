const db = require('../models');
const jwt = require('jsonwebtoken');

const unauthorizedResponse = {
  status: {
    code: 401,
    message: 'Token doesn\'t exist',
    succeeded: false
  }
};

module.exports = () =>  async (req, res, next) => {
  try {

    // validate if has headers
    const {headers} = req;

    if(!headers['authorization']) {
      return res.status(401).send(unauthorizedResponse);
    }

    const bearerLength = 'Bearer '.length;
    //token from header
    let token = headers['authorization'].slice(bearerLength) || null;

    //no token provided
    if(!token){
      return res.status(401).send(unauthorizedResponse);
    }

    const decoded =  await jwt.verify(token, process.env.SECRET_KEY);
    if (!decoded || !decoded.id) {
      return res.status(401).send(unauthorizedResponse);
    }
    let userData = await db['User'].findOne({
      where: {id: decoded.id},
    })

    const {is_validate} = req.body;
    
    if (!is_validate) {
      next({id: decoded.id, userData, validated: true});
    } else {
      return res.send({id: decoded.id, userData, validated: true})
    }

  } catch (err){
    console.log(err)
    if (next) {
      next(err);
    } else {
      return res.status(200).send(err)
    }
  }
};
